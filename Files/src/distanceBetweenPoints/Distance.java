package distanceBetweenPoints;;

public class Distance {
	public String Name;
	public int x;
	public int y;
	public Distance(String name, int x, int y) {
		super();
		Name = name;
		this.x = x;
		this.y = y;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public double getDistance(points p1,points p2)
	{
		return Math.sqrt(Math.pow(p2.getX()-p1.getY()),2)+(Math.pow(p2.getY()-p1.getY()));
	}
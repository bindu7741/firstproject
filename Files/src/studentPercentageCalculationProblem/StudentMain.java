package studentPercentageCalculationProblem;

	import java.io.FileWriter;
	import java.io.IOException;
	import java.util.ArrayList;
	import java.util.List;

	public class StudentMain {
		
		public static void main (String arg[]) throws IOException{
			StudentReader s = new StudentReader();
			List<StudentInformation> list = s.readcsvFile("student.csv");
		
			FileWriter writer = new FileWriter("percentage.csv");
			writer.write("Id,name,m1,m2,m3,percentage \n");
			for(StudentInformation st:list){
			
				writer.write(st.toString());
			}
			writer.close();
		}

	}


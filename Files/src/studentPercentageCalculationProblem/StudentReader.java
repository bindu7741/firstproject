package studentPercentageCalculationProblem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StudentReader {
	static List<StudentInformation> list = new ArrayList<StudentInformation>();

	public List<StudentInformation> readcsvFile(String filename) throws IOException {
		FileReader reader = new FileReader(filename);
		BufferedReader buffer = new BufferedReader(reader);

		String line = buffer.readLine();

		try {
			line = buffer.readLine();
			while (!line.isEmpty()) {
				String[] details = line.split(",");

				createStudent(details);

				line = buffer.readLine();
			}
		} catch (Exception e) {
			buffer.close();
			reader.close();
		}
		return list;
	}

	public void createStudent(String[] details) {

		int id = Integer.valueOf(details[0]);
		String name = details[1];
		int m1 = Integer.valueOf(details[2]);
		int m2 = Integer.valueOf(details[3]);
		int m3 = Integer.valueOf(details[4]);

		StudentInformation s = new StudentInformation(id, name, m1, m2, m3);
		list.add(s);

	}
}

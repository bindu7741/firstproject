package studentPercentageCalculationProblem;
	import java.io.FileWriter;
	import java.io.IOException;

	public class StudentWriter {
		public static void main(String arg[]) throws IOException{
			StudentInformation s1 = new StudentInformation(1,"bindu",70,75,70);
			StudentInformation s2 = new StudentInformation(2,"priya",70,79,84);
			
			
			FileWriter writer =new FileWriter("student.csv");
			writer.write("Id,Name,maths,physics,chemistry,percentage\n");
			writer.write(s1.toString());
			writer.write(s2.toString());

			writer.close();
		}

	}


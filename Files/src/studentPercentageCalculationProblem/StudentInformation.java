package studentPercentageCalculationProblem;

public class StudentInformation {
	private int id;
	private String name;
	private int m1;
	private int m2;
	private int m3;

	public static int idgenerator=1;
	public StudentInformation(int id2, String name2, int m12, int m22, int m32) {
	
		id=idgenerator++;
		this.name = name2;
		this.m1 = m12;
		this.m2 = m22;
		this.m3 = m32;
	}
	public int getId() {
		return id;
	}
	public void setId(){
		this.id=id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getM1() {
		return m1;
	}
	public void setM1(int m1) {
		this.m1 = m1;
	}
	public int getM2() {
		return m2;
	}
	public void setM2(int m2) {
		this.m2 = m2;
	}
	public int getM3() {
		return m3;
	}
	public void setM3(int m3) {
		this.m3 = m3;
	}

	public double getPercentage() {
		return (m1+m2+m3)/3;
	}
	
	public String toString() {
		return  id + "," + name + "," + m1 + ","+ m2 +"," + m3 + "," +getPercentage() +"\n";
	}
	
}



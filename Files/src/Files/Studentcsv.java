package Files;

public class Studentcsv {
	public int Id;
	public String Name;
	public float M1;
	public float M2;
	public float M3;
	public static int IdGenerator=1;
	public Studentcsv(String name, float m1, float m2, float m3) {
		Id = IdGenerator++;
		Name = name;
		M1 = m1;
		M2 = m2;
		M3 = m3;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public float getM1() {
		return M1;
	}
	public void setM1(float m1) {
		M1 = m1;
	}
	public float getM2() {
		return M2;
	}
	public void setM2(float m2) {
		M2 = m2;
	}
	public float getM3() {
		return M3;
	}
	public void setM3(float m3) {
		M3 = m3;
	}
	public  float percentage()
	{
		float percentage=((M1+M2+M3)/300)*100;
		return percentage;
	}
	@Override
	public String toString() {
		return Id + "," + Name + ","  + M1 + "," + M2 + " ,"   +M3 +","+percentage()+"\n";
		
		
				
	}
	

}

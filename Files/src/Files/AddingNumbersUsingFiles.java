package Files;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

public class AddingNumbersUsingFiles {
	public static void main(String args[])throws IOException
	{
		int n1=23,n2=21;
		FileWriter Writer=new FileWriter("output.text");
		String str="n1=" +String.valueOf(n1)+ "n2=" + String.valueOf(n2);
		Writer.write(str);
		String result="sum="+String.valueOf(n1+n2);
		Writer.append(result);
		Writer.close();
		
	FileReader Reader=new FileReader("output.text");
	int c;
	while((c=Reader.read())!=-1){
	System.out.print((char) c);

	}

	}
}

package Files;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriterForStudentSpreadSheet {
	public static void main(String args[]) throws IOException
	{
		Studentcsv s1=new Studentcsv("bindu",67,78,68);
		Studentcsv s2=new Studentcsv("sindu",97,58,48);
	
		FileWriter w=new FileWriter("StudentSheet.csv");
		w.write("id,name,m1,m2,m3,percentage\n");
		w.write(s1.toString());
		w.write(s2.toString());
		w.close();
}

		
}

package first;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class ClientApp {
	public static void main(String args[])
	{
		Configuration cfg=new AnnotationConfiguration().configure("Hibernate.cfg.xml"); 
		//cfg is loaded
		SessionFactory sf=cfg.buildSessionFactory();
		//creating sf object
		Session se=sf.openSession();
		//connection is established
		Transaction tx=se.beginTransaction();
		Student st=new Student();
		st.setName("priya");
		st.setMarks(610);
		se.save(st);
		tx.commit();
		se.close();
		
	}

}




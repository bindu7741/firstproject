package studentpercentageproblem;

public abstract class Student {
	private double id;
	private String name;
	private static int Idgenerator=101;
	public Student( String name) {
		id=Idgenerator++;
		
		this.name = name;
	}
	public abstract double findPercentage();
	
	public double getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static int getIdgenerator() {
		return Idgenerator;
	}
	public static void setIdgenerator(int idgenerator) {
		Idgenerator = idgenerator;
	}
	public String getGrade(double Percentage)
	{
	if(Percentage>90)
	{
		return "A";
	}else
		return "B";
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + "";
	}
}

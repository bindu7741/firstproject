package studentpercentageproblem;

public class class8 extends Student  {
	private double sub1;
	private double sub2;
	private double sub3;
	private double sub4;
	private double sub5;
	public class8(String name,double sub1, double sub2, double sub3, double sub4, double sub5) {
		super(name);
		this.sub1 = sub1;
		this.sub2 = sub2;
		this.sub3 = sub3;
		this.sub4 = sub4;
		this.sub5 = sub5;
	}
	public double getSub1() {
		return sub1;
	}
	public void setSub1(double sub1) {
		this.sub1 = sub1;
	}
	public double getSub2() {
		return sub2;
	}
	public void setSub2(double sub2) {
		this.sub2 = sub2;
	}
	public double getSub3() {
		return sub3;
	}
	public void setSub3(double sub3) {
		this.sub3 = sub3;
	}
	public double getSub4() {
		return sub4;
	}
	public void setSub4(double sub4) {
		this.sub4 = sub4;
	}
	public double getSub5() {
		return sub5;
	}
	public void setSub5(double sub5) {
		this.sub5 = sub5;
	}
	public double findPercentage()
	{
		return ((sub1+sub2+sub3+sub4+sub5)/500)*100;
		
	}
	@Override
	public String toString() {
		return "class8 ["+super.toString()+"Grade:"+getGrade(findPercentage())+" sub1=" + sub1 + ", sub2=" + sub2 + ", sub3=" + sub3 + ", sub4=" + sub4 + ", sub5=" + sub5 +",percentage="+findPercentage()+"]";
	}
	
}

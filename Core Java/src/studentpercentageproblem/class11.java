package studentpercentageproblem;

public class class11 extends Student {
	private double PTheory;
	private double CTheory;
	private double MTheory;
	private double English;
	private double Hindi;
	private double PPractical;
	private double CPractical;
	private double MPractical;
	public class11(String name,double pTheory, double cTheory, double mTheory, double english, double hindi, double pPractical,
			double cPractical, double mPractical) {
		super(name);
		PTheory = pTheory;
		CTheory = cTheory;
		MTheory = mTheory;
		English = english;
		Hindi = hindi;
		PPractical = pPractical;
		CPractical = cPractical;
		MPractical = mPractical;
	}
	public double getPTheory() {
		return PTheory;
	}
	public void setPTheory(double pTheory) {
		PTheory = pTheory;
	}
	public double getCTheory() {
		return CTheory;
	}
	public void setCTheory(double cTheory) {
		CTheory = cTheory;
	}
	public double getMTheory() {
		return MTheory;
	}
	public void setMTheory(double mTheory) {
		MTheory = mTheory;
	}
	public double getEnglish() {
		return English;
	}
	public void setEnglish(double english) {
		English = english;
	}
	public double getHindi() {
		return Hindi;
	}
	public void setMaths(double hindi) {
		Hindi = hindi;
	}
	public double getPPractical() {
		return PPractical;
	}
	public void setPPractical(double pPractical) {
		PPractical = pPractical;
	}
	public double getCPractical() {
		return CPractical;
	}
	public void setCPractical(double cPractical) {
		CPractical = cPractical;
	}
	public double getMPractical() {
		return MPractical;
	}
	public void setMPractical(double mPractical) {
		MPractical = mPractical;
	}
	public double findPercentage()
	{
		return (((PTheory+CTheory+MTheory+English+Hindi)/500*100)+((PPractical+CPractical+MPractical)/300*100));
	}
	
	@Override
	public String toString() {
		return "class11 ["+super.toString()+"PTheory=" + PTheory + ", CTheory=" + CTheory + ", MTheory=" + MTheory + ", English=" + English
				+ ", Hindi=" + Hindi + ", PPractical=" + PPractical + ", CPractical=" + CPractical + ", MPractical="
				+ MPractical + ", findPercentage()=" + findPercentage() + "]";
	
	}
}

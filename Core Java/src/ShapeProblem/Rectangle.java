package ShapeProblem;

public class Rectangle extends shape {
	protected double width;
	protected double length;
	
	public Rectangle()
	{
	
	}

	public Rectangle(double width,double length)
	{
	this.width = width;
	this.length = length;
	}
	
	public Rectangle(double width,double length,String color,boolean filled)
	{
	}
	
	public double getWidth() {
	return width;
	}
		
	public void setWidth(double width) {
	this.width = width;
	}
	
	public double getLength() {
	return length;
		}
	
	public void setLength(double length) {
	this.length = length;
	}
	
	public double getArea(){
	return length*width;
	}
	
	public double getPerimeter()
	{
	return 2*(length+width);
	}
	
	public String toString()
	{
	return " "+super.toString()+" length:"+length+" width:"+width+" Area:"+getArea()+" Perimeter:"+getPerimeter()+"";
	}
}

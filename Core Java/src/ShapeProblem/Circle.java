package ShapeProblem;

public class Circle extends shape {
	protected double radius;
	public Circle()
	{
		
	}
	
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	public Circle(double radius,String color,boolean filled)
	{
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea()
	{
		return 3.14 * radius * radius;
	}
	
	public double getPerimeter()
	{
		return 2 * 3.14 * radius;
	}
	
	public String toString()
	{
		return " "+super.toString()+" radius:"+radius+" Area:"+getArea()+" perimeter:"+getPerimeter()+"";
	}
	

}

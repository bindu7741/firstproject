package interfaceproblem;

public class Circle implements Shape {
	private double radius;
public Circle(double radius)
{
	this.radius=radius;
	
}
public double getRadius() {
	return radius;
}
public void setRadius(double radius) {
	this.radius = radius;
}
public double calcArea()
{
	return 3.14*radius*radius;
}
public String toString()
{
	return "Circle[ radius="+radius+" Area="+calcArea()+" ]";
}
}


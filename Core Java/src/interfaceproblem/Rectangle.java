package interfaceproblem;

public class Rectangle implements Shape,Perimeter {
	private double length;
	private double breadth;
	public Rectangle(double length,double breadth)
	{
		this.length=length;
		this.breadth=breadth;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getBreadth() {
		return breadth;
	}
	public void setBreadth(double breadth) {
		this.breadth = breadth;
	}
	public double calcArea()
	{
		return length *	breadth;
	}
	public double calcPerimeter()
	{
		return 2*(length+breadth);
		
	}
	public String toString()
	{
		return "Rectangle[length="+length+" breadth= "+breadth+" Area="+calcArea()+" perimeter="+calcPerimeter()+"]";
	}
	

}

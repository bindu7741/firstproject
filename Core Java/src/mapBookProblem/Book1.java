package mapBookProblem;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Book1 {
	public static void main(String args[]) {
		Book b1 = new Book("2states", "Chethan bhagath");
		Book b2 = new Book("wings of fire", "Abdul kalam");
		Book b3 = new Book("Revolution", "Bindu");
		Map<Integer, Book> M1 = new HashMap<Integer, Book>();
		M1.put(b1.getId(), b1);
		M1.put(b2.getId(), b2);
		M1.put(b3.getId(), b3);
		for (Map.Entry<Integer, Book> entry : M1.entrySet()) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		getSorting(M1);
	}

	public static void getSorting(Map<Integer, Book> M1) {
		List<Map.Entry<Integer, Book>> MapList1 = new ArrayList<Map.Entry<Integer, Book>>();

		
		for (Map.Entry<Integer, Book> entry : M1.entrySet()) {
			MapList1.add(entry);
		}
		for (Map.Entry<Integer, Book> entry : MapList1) {
			System.out.println(entry);
		}
		Collections.sort(MapList1, new BookMain());
		System.out.println("============");
		for (Map.Entry<Integer, Book> entry : MapList1) {
			System.out.println(entry);
		}

	}
}

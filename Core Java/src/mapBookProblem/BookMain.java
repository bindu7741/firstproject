package mapBookProblem;

import java.util.Map;

import java.util.Comparator;

public class BookMain implements Comparator<Map.Entry<Integer, Book>> {
	public int compare(Map.Entry<Integer, Book> entry1, Map.Entry<Integer, Book> entry2) {
		return entry1.getValue().getAuthor().compareTo(entry2.getValue().getAuthor());
	}
}

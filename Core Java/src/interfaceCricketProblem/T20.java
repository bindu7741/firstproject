package interfaceCricketProblem;

public class T20 implements Cricket {
	private double innings;
	private double overs_per_innings;
	private double runs_per_innings;
	private double fall_of_wickets;
	
	public T20(double innings, double overs_per_innings, double runs_per_innings, double fall_of_wickets) {
		super();
		this.innings = innings;
		this.overs_per_innings = overs_per_innings;
		this.runs_per_innings = runs_per_innings;
		this.fall_of_wickets = fall_of_wickets;
	}
	public double getInnings() {
		return innings;
	}
	public void setInnings(double innings) {
		this.innings = innings;
	}
	public double getOvers_per_innings() {
		return overs_per_innings;
	}
	public void setOvers_per_innings(double overs_per_innings) {
		this.overs_per_innings = overs_per_innings;
	}
	public double getRuns_per_innings() {
		return runs_per_innings;
	}
	public void setRuns_per_innings(double runs_per_innings) {
		this.runs_per_innings = runs_per_innings;
	}
	public double getFall_of_wickets() {
		return fall_of_wickets;
	}
	public void setFall_of_wickets(double fall_of_wickets) {
		this.fall_of_wickets = fall_of_wickets;
	}
	public double averageScore()
	{
		return  runs_per_innings/innings;
	}
	public double averageFallOfWickets()
	{
		return fall_of_wickets/overs_per_innings;
	}
	public double calcStrikeRate()
	{ 
		return (runs_per_innings/overs_per_innings)*100;
	}
	public String toString()
	{
		return "T20[innings = "+innings+" overs_per_innings = "+overs_per_innings+"fall_of_wickets ="+fall_of_wickets+" averageScore="+ averageScore()+"averageFallOfWickets="+averageFallOfWickets()+"calcStrikeRate="+calcStrikeRate()+"" ;
	}
	



}

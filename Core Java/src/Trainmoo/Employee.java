package Trainmoo;

public class Employee {
	private int Id;
	private String FirstName;
	private String LastName;
	private int Salary;
	private static int Idgenerator=101;
	//private String Name;
	public Employee(String FirstName,String LastName,int Salary)
	{	
		Id=Idgenerator++;
		this.FirstName=FirstName;
		this.LastName=LastName;
		this.Salary=Salary;
		//this.Name=Name;
	}
	public int getAnnualSalary()
	{
		return Salary*12;
	
	}
	public int getId()
	{
	return Id;
	}
	public String getFirstName()
	{
	return FirstName;
	}
	public String getLastName()
	{
	return LastName;
	}
	public int getSalary()
	{
	return Salary;
	}
	public String getName()
	{
		return FirstName.concat(LastName);
		
	}
	public int raiseSalary(int percent)
	{
		int raiseSalaryPercent=(Salary*percent)/100;
		int raiseSalary=Salary+raiseSalaryPercent;
		return raiseSalary; 
	}
	
	public String toString()
	{
		return"Employee[ Id:"+Id+",Name:"+getName()+",salary:"+Salary+"AnnualSalary:"+getAnnualSalary()+"]";
	}
	}

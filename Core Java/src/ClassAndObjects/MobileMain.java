package ClassAndObjects;

public class MobileMain {
	public static void main(String args[])
	{
	//classname objectname=new classname();
	Mobile Samsung=new Mobile();
	//objectname.membername
	Samsung.RAM=6;
	Samsung.ROM=64;
	Samsung.Display=5.5;
	Samsung.OS="Android9";
	System.out.println("Samsung:");
	System.out.println("Ram:"+Samsung.RAM);
	System.out.println("Rom:"+Samsung.ROM);
	System.out.println("Display:"+Samsung.Display);
	System.out.println("OS:"+Samsung.OS);
	Samsung.calling();
	Samsung.browsing();
	Mobile MI=new Mobile();
	//objectname.membername
	MI.RAM=8;
	MI.ROM=32;
	MI.Display=5.2;
	MI.OS="Android9";
	System.out.println("MI:");
	System.out.println("Ram:"+MI.RAM);
	System.out.println("Rom:"+MI.ROM);
	System.out.println("Display:"+MI.Display);
	System.out.println("OS:"+MI.OS);
	 MI.calling();
	MI.browsing();
	
	Demo d=new Demo();
	d.n1=25;
	d.n2=34;
	d.add();
	}
	

}

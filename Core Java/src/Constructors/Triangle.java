package Constructors;

public class Triangle {
	private float side1;
	private float side2;
	private float side3;
	public Triangle()
	{
		System.out.println("Area of triangle:");
	}
	public Triangle(float side1,float side2,float side3)
	{
		this.side1=side1;
		this.side2=side2;
		this.side3=side3;
	}
	public void setside1(float side1)
	{
		this.side1=side1;
	}
	public void setside2(float side2)
	{
		this.side2=side2;
	}
	public void setside3(float side3)
	{
		this.side3=side3;
	}
	
	public float getside1()
	{
		return side1;
	}
	public float getside2()
	{
		return side2;
	}
	public float getside3()
	{
		return side3;
	}
	public  double getSValue()
	{
		float s=(side1+side2+side3)/2;
		double area=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
		return area;
}
	public String toString()
	{
		return "side1:"+side1+" \nside2:"+side2+" \nside3:"+side3+"\narea:"+getSValue()+"";
	}
}

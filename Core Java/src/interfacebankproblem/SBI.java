package interfacebankproblem;

public class SBI implements Bank{
	private double rateofInterest;
	private Customer customer;
	
	public SBI( Customer customer) {
		super();
		
		this.customer = customer;
	}
	public double getrateofInterest() {
		return rateofInterest;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public double calcROI()
	{
		rateofInterest=(customer.getTenure()/customer.getInvestment())*100;
		return rateofInterest;
	}
	@Override
	public String toString() {
		return "SBI [rateofInterest=" + calcROI()+ ", customer=" + customer + "]";
	}
	
	}
	
	



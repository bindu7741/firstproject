package interfacebankproblem;

public class Customer {
	
	private double investment;
	private double tenure;
	public Customer(double tenure, double investment) {
		super();
		this.investment = investment;
		this.tenure = tenure;
	}
	public double getInvestment() {
		return investment;
	}
	public void setInvestment(double investment) {
		this.investment = investment;
	}
	public double getTenure() {
		return tenure;
	}
	public void setTenure(double tenure) {
		this.tenure = tenure;
	}
	@Override
	public String toString() {
		return "Customer [investment=" + investment + ", tenure=" + tenure + "]";
	}

}

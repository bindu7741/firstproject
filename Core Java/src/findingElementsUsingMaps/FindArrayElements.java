package findingElementsUsingMaps;
public class FindArrayElements {
	public static void main(String[] args) 
    { 
        int arr[] = new int[]{ 1,1,2,1,3,2,3,4,4,3,4,4,6 }; 
       int n = arr.length; 
        System.out.println(getOddOccurrence(arr, n)); 
    } 

		public static int getOddOccurrence(int arr[], int arr_size) 
	    { 
	        int i; 
	        for (i = 0; i < arr_size; i++) { 
	            int count = 0; 
	            for (int j = 0; j < arr_size; j++) { 
	                if (arr[i] == arr[j]) 
	                    count++; 
	            } 
	        
	            if (count % 2 != 0)
	            {
	                return arr[i]; 
	        } 
	        }
	        return -1; 
	    } 
	}
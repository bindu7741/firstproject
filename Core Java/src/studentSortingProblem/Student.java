package studentSortingProblem;

public class Student {

	private int id;
	private String name;
	private String Address;
	private float subject1;
	private float subject2;
	private float subject3;
	private static int idgenerator = 101;

	public Student(String name, String address, float subject1, float subject2, float subject3) {
		super();
		this.id = idgenerator++;
		this.name = name;
		Address = address;
		this.subject1 = subject1;
		this.subject2 = subject2;
		this.subject3 = subject3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public float getSubject1() {
		return subject1;
	}

	public void setSubject1(float subject1) {
		this.subject1 = subject1;
	}

	public float getSubject2() {
		return subject2;
	}

	public void setSubject2(float subject2) {
		this.subject2 = subject2;
	}

	public float getSubject3() {
		return subject3;
	}

	public void setSubject3(float subject3) {
		this.subject3 = subject3;
	}

	/*
	 * public int getIdgenerator() { return idgenerator; } public void
	 * setIdgenerator(int idgenerator) { this.idgenerator = idgenerator; }
	 */
	public float getPercentage() {
		return (subject1 + subject2 + subject3) / 300 * 100;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", subject1=" + subject1 + ", subject2=" + subject2
				+ ", subject3=" + subject3 + ", percentage:" + getPercentage() + "]";

	}

}

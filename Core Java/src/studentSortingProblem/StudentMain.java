package studentSortingProblem;
import java.util.List;

import listsMovieProblem.Movie;
import listsMovieProblem.RatingComparator;

import java.util.ArrayList;
import java.util.Collections;
public class StudentMain {


public static void main(String args[])
{
		Student s1=new Student("Bindu","Gachibowli",56,45,67);
		Student s2=new Student("Priya","kukatpally",43,47,89);
		Student s3=new Student("chandhu","Miyapur",78,54,34);
		List <Student>list1=new ArrayList<Student>();
		list1.add(s1);
		list1.add(s2);
		list1.add(s3);
		/*for(int i=0;i<list1.size();i++)
		
		System.out.println(list1.get(i));*/
		for(Student s:list1)
		{
			System.out.println(s);
		}
		Collections.sort(list1,new IdComparator());
		System.out.println("=======");
		System.out.println("Id Sorting:");
		for(Student m:list1)
		{
			System.out.println(m);
		}
		Collections.sort(list1,new PercentageComparator());
		System.out.println("=======");
		System.out.println("Percentage Sorting:");
		for(Student m:list1)
		{
			System.out.println(m);
		}
		
	}

}

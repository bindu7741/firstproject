package CricketProblem;

public class Batsman extends Player{
	private int runsScored;
	private int centuries;
	private int halfCenturies;
	private int ballsFaced;
	private int sixes;
	private int fours;
	public Batsman(String name,int runsScored,int centuries,int halfCenturies,int ballsFaced,int sixes,int fours)
	{
		super(name);
		this.runsScored=runsScored;
		this.centuries=centuries;
		this.halfCenturies=halfCenturies;
		this.ballsFaced=ballsFaced;
		this.sixes=sixes;
		this.fours=fours;
		
	}
	public int getrunScored()
	{
		return runsScored;
	}
	
	
	public int getballsFaced()
	{
		return ballsFaced;
	}
	public int getsixes()
	{
		return sixes;
	}
	public int getfours()
	{
		return fours;
	}
	public float getStrikeRate()
	{
		return (runsScored*100)/ballsFaced;
		
	}
	public int getRunsScoredInBoundaries()
	{
		return (4*fours+6*sixes);
	}
	public String toString()
	{
		return "Batsman["+super.toString()+"RunsScored="+runsScored+"centuries"+centuries+"halfcenturies="+halfCenturies+"Ballsfaced="+ballsFaced+"sixes="+sixes+"fours="+fours+"StrikeRate="+getStrikeRate()+"RunsScoredInBoundaries="+getRunsScoredInBoundaries()+"]";
	}


}

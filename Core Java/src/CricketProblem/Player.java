package CricketProblem;

public class Player {
	private int id;
	private String name;
	private static int idgenerator=101;
	public Player()
	{
	}
	public Player(String name)
	{
	id=idgenerator++;
	this.name=name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + "]";
	}
	
		

}

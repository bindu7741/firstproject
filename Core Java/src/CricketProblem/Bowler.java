package CricketProblem;

public class Bowler {
	private int ballsBowled;
	private int runsLeaked;
	private int wickets;

	public Bowler(int ballsBowled, int runsLeaked,int wickets)
	{
		this.ballsBowled=ballsBowled;
		this.runsLeaked=runsLeaked;
		this.wickets=wickets;
	}

	public int getBallsBowled() {
		return ballsBowled;
	}

	public void setBallsBowled(int ballsBowled) {
		this.ballsBowled = ballsBowled;
	}

	public int getRunsLeaked() {
		return runsLeaked;
	}

	public void setRunsLeaked(int runsLeaked) {
		this.runsLeaked = runsLeaked;
	}

	public int getWickets() {
		return wickets;
	}

	public void setWickets(int wickets) {
		this.wickets = wickets;
	}
	public int getStrikeRate()
	{
		return  ( ballsBowled / wickets) ;
	}
	public String toString()
	{
		return "Bowler[ballsBowled="+ballsBowled+"runsLeaked="+runsLeaked+"wickets ="+wickets+"StrikeRate="+getStrikeRate()+"]";
}

}

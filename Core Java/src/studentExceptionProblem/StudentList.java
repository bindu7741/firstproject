package studentExceptionProblem;

import java.util.*;
import productExceptionProblem.InvalidProductException;
import productExceptionProblem.Product;

public class StudentList {
	public static void main(String args[]) throws StudentNotFoundException 

	{
		
		Student s1 = new Student("bindu", "Gachibowli");
		Student s2 = new Student("prasanna", "nuzwid");
		Student s3 = new Student("cherry", "nellore");
		Student s4 = new Student("sharuu", "nizamabad");
		Student s5 = new Student("bharath", "nizamabad");
		Student s6 = new Student("lavan", "warangal");
		Student s7 = new Student("vijaya", "nuzwid");
		Student s8 = new Student("kalyani", "kothaguda");
		Student s9 = new Student("ramya", "tirupati");
		Student s10 = new Student("yogitha", "nuzwid");
		ArrayList<Student> list1 = new ArrayList<Student>();
		list1.add(s1);
		list1.add(s2);
		list1.add(s3);
		list1.add(s4);
		list1.add(s5);
		list1.add(s6);
		list1.add(s7);
		list1.add(s8);
		list1.add(s9);
		list1.add(s10);
		Scanner sc = new Scanner(System.in);
		int id = sc.nextInt();
		Iterator<Student> it = list1.iterator();
		Student s=null;
		while (it.hasNext()) {
			Student std = it.next();
				if (std.getId() == id) {
					s=std;
				}
		}
				if(s==null){
					throw new StudentNotFoundException("Student details not found");
				}
				else
					System.out.println(s);
				}
					


	}



package EmployeeListProblem;
import java.io.*;

public class Employee implements Comparable<Employee> {
	private int id;
	private String Name;
	private double salary;
	private static int idgenerator=101;
	
	public Employee(String name, float salary) {
		id=idgenerator++;
		this.Name = name;
		this.salary = salary;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", Name=" + Name + ", salary=" + salary + "";
				
	
	}	
	public int compareTo(Employee emp)
	{
		 if(salary>emp.getSalary())
		 return 1;
		 else if(salary<emp.getSalary())
			 return -1;
		 return 0;
	}

}

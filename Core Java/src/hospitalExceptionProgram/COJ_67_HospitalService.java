package hospitalExceptionProgram;
import java.util.*;
public class COJ_67_HospitalService {
	static ArrayList<COJ_67_Hospital>list2=new ArrayList<COJ_67_Hospital>();
	public static int addHospital(COJ_67_Hospital h1)
	{
		list2.add(h1);
		return h1.getHospitalCode();
	}
	public static Map<Integer,String> gethospitals(){
	
		Map<Integer,String>m1=new HashMap<Integer,String>();
		for(COJ_67_Hospital e:list2)
		{
			m1.put(e.getHospitalCode(), e.getHospitalName());
		}
		return m1;
	}
	public static COJ_67_Hospital getHospitalDetails(int code)
	{
		for(COJ_67_Hospital f:list2)
		{
			if(f.getHospitalCode()==code){
				return f;
			}
			
		}
		return null;
	}
	
		
}

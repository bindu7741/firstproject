package hospitalExceptionProgram;
import java.util.*;
public class COJ_67_Hospital {
	private int hospitalCode;
	private String  hospitalName;
	private static int hospitalCode1=1000;
	private String contactperson;
	private String contactnumber;
	private String location;
	List<String>listOfTreatments;
	
	public COJ_67_Hospital(String hospitalName, String contactperson, String contactnumber,
			String location, List<String> listOfTreatments) {
		hospitalCode = hospitalCode1++;
		this.hospitalName = hospitalName;
		this.contactperson = contactperson;
		this.contactnumber = contactnumber;
		this.location = location;
		this.listOfTreatments = listOfTreatments;
	}

	public String getContactperson() {
		return contactperson;
	}

	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}

	public String getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<String> getListOfTreatments() {
		return listOfTreatments;
	}

	public void setListOfTreatments(List<String> listOfTreatments) {
		this.listOfTreatments = listOfTreatments;
	}

	public int getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(int hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	@Override
	public String toString() {
		return "COJ_67_Hospital [hospitalCode=" + hospitalCode + ", hospitalName=" + hospitalName + ", contactperson="
				+ contactperson + ", contactnumber=" + contactnumber + ", location=" + location + ", listOfTreatments="
				+ listOfTreatments + "]";
	}

	
	
}

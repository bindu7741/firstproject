package productExceptionProblem;

import java.util.*;

import EmployeeListProblem.Employee;

public class ProductList {
	public static void main(String args[]) throws InvalidProductException {
		Product p1 = new Product(323, 34);
		Product p2 = new Product(234, 45);
		Product p3 = new Product(121, 23);
		Product p4 = new Product(454, 67);
		Product p5 = new Product(141, 89);
		Product p6 = new Product(67, 12);
		Product p7 = new Product(90, 34);
		Product p8 = new Product(457, 234);
		Product p9 = new Product(321, 342);
		Product p10 = new Product(123, 126);
		ArrayList<Product> list1 = new ArrayList<Product>();
		list1.add(p1);
		list1.add(p2);
		list1.add(p3);
		list1.add(p4);
		list1.add(p5);
		list1.add(p6);
		list1.add(p7);
		list1.add(p8);
		list1.add(p9);
		list1.add(p10);
		ArrayList<Product> newProductList = new ArrayList<Product>();

		Iterator<Product> it = list1.iterator();
		while (it.hasNext()) {
			Product prod = it.next();
			try {
				if (prod.getWeight() < 200) {
					throw new InvalidProductException();
				}

			} catch (InvalidProductException e) {

				it.remove();

			}
		}

		
		System.out.println(list1);
	}
}
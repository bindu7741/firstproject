package productExceptionProblem;

public class Product {
	public int id;
	public double weight;
	public double price;
	public static int idgenerator=101;
	public Product(double weight, double price) {
		id=idgenerator++;
		this.weight = weight;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", weight=" + weight + ", price=" + price + "]";
	}
	
}

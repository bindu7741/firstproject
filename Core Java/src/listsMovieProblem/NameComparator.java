package listsMovieProblem;
import java.util.Comparator;

public class NameComparator implements Comparator<Movie> {
	public int compare(Movie M1,Movie M2){  
		return M1.getName().compareTo(M2.getName());
	}  
}

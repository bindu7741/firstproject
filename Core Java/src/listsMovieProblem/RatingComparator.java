package listsMovieProblem;
import java.util.Comparator;
public class RatingComparator implements Comparator<Movie> {
	public int compare(Movie M1,Movie M2)
	{
		if(M1.getRating()>M2.getRating())
			return 1;
		else if(M1.getRating()<M2.getRating())
			return -1;
		return 0;
	
	}

}



package listsMovieProblem;
	import java.util.List;
	import java.util.ArrayList;
import java.util.Collections;
	public class Movie1 {
		public static void main(String args[])
		{
			Movie M1=new Movie("Majili","prathap",130,3.0);
			Movie M2=new Movie("Manam","prathyu",120,4.0);
			Movie M3=new Movie("Bahubali","rajmouli",150,6.0);
			List<Movie> list1=new ArrayList<Movie>();
			
			list1.add(M1);
			list1.add(M2);
			list1.add(M3);
			for(Movie m:list1)
			{
				System.out.println(m);
			}
			Collections.sort(list1,new NameComparator());
			System.out.println("=======");
			System.out.println("Name Sorting:");
			for(Movie m:list1)
			{
				System.out.println(m);
			}
			Collections.sort(list1,new DirectorNameComparator());
			System.out.println("=======");
			System.out.println("Director Name Sorting:");
			for(Movie m:list1)
			{
				System.out.println(m);
			}
			
			Collections.sort(list1,new RatingComparator());
			System.out.println("=======");
			System.out.println("Rating Sorting:");
			for(Movie m:list1)
			{
				System.out.println(m);
			}
			Collections.sort(list1,new DurationComparator());
			System.out.println("=======");
			System.out.println("Duration Sorting:");
			for(Movie m:list1)
			{
				System.out.println(m);
			}
			
			
		}

	}


package listsMovieProblem;
import java.io.*;

public class Movie {
		private String name;
		private String director_name;
		private double duration;
		private double rating;
		
		public Movie(String name, String director_name, double d, double e) {
			this.name = name;
			this.director_name = director_name;
			this.duration = d;
			this.rating = e;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setDirector_name(String director_name) {
			this.director_name = director_name;
		}
		public void setDuration(float duration) {
			this.duration = duration;
		}
		public void setRating(float rating) {
			this.rating = rating;
		}
		public String getName() {
			return name;
		}
		public String getDirector_name() {
			return director_name;
		}
		public double getDuration() {
			return duration;
		}
		public double getRating() {
			return rating;
		}
		
		@Override
		public String toString() {
			return "Movie [name=" + name + ", director_name=" + director_name + ", duration=" + duration + ", rating="
					+ rating + "]";
		}
		
}

package listsMovieProblem;
import java.util.Comparator;

	public class DirectorNameComparator implements Comparator<Movie> {
		public int compare(Movie M1,Movie M2){  
			return M1. getDirector_name().compareTo(M2. getDirector_name());
		}  
	}


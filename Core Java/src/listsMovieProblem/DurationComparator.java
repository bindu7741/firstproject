package listsMovieProblem;
import java.util.Comparator;

public class DurationComparator implements Comparator<Movie> {
	public int compare(Movie M1,Movie M2)
	{
		if(M1.getDuration()>M2.getDuration())
			return 1;
		else if(M1.getDuration()<M2.getDuration())
			return -1;
		return 0;
	
	}

}

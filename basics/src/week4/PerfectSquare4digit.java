package week4;

public class PerfectSquare4digit {
	public static void main(String args[])
	{
		System.out.println(getSquare());
	}
	public static String getSquare()
	{
		String s="";
		for(int i=32;i<99;i++)
		{
			int num=i*i;
			if(getAllEvenDigits(num))
			{
				s=s+num+" , ";
			}
		}
		return s;	
	}
	public static boolean getAllEvenDigits(int n)
	{
		int t;
		if(n==0)
			return false;
		while(n!=0)
		{
			t=n%10;
			n=n/10;
			if(t%2!=0)
			return false;
			
		}
		return true;
	}
}



package week4;
import java.util.Scanner;

public class TwinPrimes {
	public static void main(String[] args) {
        int num1 = 1;
        int num2 = 200;
        System.out.println(twinPrimes(num1, num2));
    }

    public static String twinPrimes(int start, int limit) {
        //ADD YOUR CODE HERE
	String res="";
		if(start<=0&&limit<=0)
			return "-1";
		else if(start>=limit)
			return "-2";
		else
		{
		for(int i=start+1;i<limit;i++)
		{
		  if(isPrime(i)&&isPrime(i+2))
     		     res=res+(i)+":"+(i+2)+",";
		}
		}
		if(res.isEmpty())
			return "-3";
		return res.substring(0,res.length()-1);
		
		}
		//return res;
    

    public static boolean isPrime(int num) {
	for(int j=2;j<num;j++)
	{
	if(num%j==0)
	return false;
	}
	return true;
		
}
}

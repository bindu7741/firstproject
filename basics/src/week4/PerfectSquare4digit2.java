package week4;

public class PerfectSquare4digit2 {
		public static void main(String[] args) {
		System.out.println(getPerfectSquareWithAllDigitsEven(200, 500));
		}

		public static String getPerfectSquareWithAllDigitsEven(int start, int limit) {
		String s = "";

		int loopStart = (int) Math.ceil(Math.sqrt(start));
		int loopEnd = (int) Math.sqrt(limit) - 1;

		for (int i = loopStart; i < loopEnd; i++) {
		if (isAllEvenDigits(i * i)) {
		s = s + (i * i) + ",";
		}
		}

		return s;
		}

		public static boolean isAllEvenDigits(int n) {
		int t;
		if (n == 0)
		return false;

		while (n != 0) {
		t = n % 10;
		n = n / 10;

		if (t % 2 != 0)
		return false;
		}

		return true;
		}
		}



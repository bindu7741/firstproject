package week7;

import java.util.*;

public class Othello {
	public static void main(String args[])
	{
char game[][] = initializeGame();
displayOthello(game);
char turn = 'w';
displayMoves(turn, game);
}

	public static void displayMoves(char turn, char[][] g) {
		List<int[]> p = new ArrayList<int[]>();

		int i = 0;
		while (i <= 1) {
			p = possibleMoves(turn, g);

			char possiblecomand = 'L';

			if (turn == 'w')
				turn = 'b';
			if (turn == 'b')
				turn = 'w';
			i++;
		}
		for (int[] move : p) {
			System.out.println("(" + move[0] + "," + move[1] + ")" + " ");
		}

	}

	public static char[][] initializeGame() {

		char arr[][] = new char[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = '-';
			}
		}
		arr[3][3] = 'w';
		arr[3][4] = 'b';
		arr[4][3] = 'b';
		arr[4][4] = 'w';
		return arr;
	}

	public static void displayOthello(char game[][]) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < game[i].length; j++) {
				System.out.print(game[i][j]);

			}
			System.out.println();
		}
	}

	public static List<int[]> possibleMoves(char turn, char g[][]) {
		int[] arr = new int[2];
		int[] arr1 = new int[2];
		int[] arr2 = new int[2];
		int[] arr3 = new int[2];

		List<int[]> l = new ArrayList<int[]>();
		char color = '\0';
		if (turn == 'w') {
			color = 'b';
		}
		if (turn == 'b') {
			color = 'w';
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (g[i][j] == turn) {
					if ((g[i + 1][j] == color) && (g[i + 2][j] == '-')) {

						arr[0] = i + 2;
						arr[1] = j;

						l.add(arr);

					}
					if ((g[i - 1][j] == color) && (g[i - 2][j] == '-')) {
						arr1[0] = i - 2;
						arr1[1] = j;
						l.add(arr1);

					}
					if ((g[i][j - 1] == color) && (g[i][j - 2] == '-')) {
						arr2[0] = i;
						arr2[1] = j - 2;
						l.add(arr2);

					}
					if ((g[i][j + 1] == color) && (g[i][j + 2] == '-')) {
						arr3[0] = i;
						arr3[1] = j + 2;
						l.add(arr3);

					}

				}

			}

		}
		return l;
	}
	public static char[][] updatePuzzle(char g[][],char turn) 
	{
	
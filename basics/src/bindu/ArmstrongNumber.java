package bindu;

public class ArmstrongNumber {
	    public static void main(String[] args) {
	        int num1 = 100;
	        int num2 = 125;
	        System.out.println(generateArmstrongNums(num1, num2));
	    }
	    
	    public static String generateArmstrongNums(int start, int limit) {
	        //ADD YOUR CODE HERE
	    	String result="";
	    	if(start<=0||limit<=0)
	    		return "-1";
	    	else if(start>=limit)
	    		return "-2";
	    	else
	    	{
	    		for(int i=start;i<limit;i++)
	    		{
	    			if(isArmstrong(i))
	    				result=result+i+",";
	    			
	    		}
	    	}
	    	if(result.isEmpty())
	    		return "-3";
	    	return result.substring(0, result.length()-1);
	    }

	    public static boolean isArmstrong(int num) {
	        //ADD YOUR CODE HERE
	    	return num==sumOfPowersOfDigits(num);
	    }

	    public static int sumOfPowersOfDigits(int n) {
	        //ADD YOUR CODE HERE
	    	int sum=0;
	    	int digits[]=getDigits(n);
	    	for(int i=0;i<digits.length;i++)
	    	{
	    	int powervalue=power(digits[i],digits.length);
	    	sum+=powervalue;
	    	}
	    	return sum;
	    }

	    public static int[] getDigits(int n) {
	        //ADD YOUR CODE HERE
	    	int temp=n,digitcount=0;
	    	while(temp>0)
	    	{
	    	temp/=10;	
	    	}
	    	int digits[]=new int[digitcount];
	    	int i=0;
	    	while(n>0)
	    	{
	    		int digit=n%10;
	    		digits[i]=digit;
	    		i++;
	    	}
	    return digits;	
	    }

	    public static int power(int d, int p) {
	        //ADD YOUR CODE HERE
	    	double pow=Math.pow(d,p);
	    	int power=(int)pow;
	    	return power;
	    }
	}




package bindu;

import java.util.Scanner;

public class SumOfUniqueElements {
	public static void main(String args[]) {
		int elements[] = { 4, 5, 6, 7, 2, 9, 6, 8 };
		// GetNumbers(elements);
		System.out.println(GetNumbers(elements));

	}

	public static int GetNumbers(int[] elements)

	{
		int sum = 0;
		for (int i = 0; i < elements.length; i++) {
			int count = 0;
			for (int j = 0; j < elements.length; j++) {
				if (elements[i] == elements[j])
					count++;
			}
			if (count == 1) {
				sum += elements[i];
			}

		}
		return sum;

		
	}
}

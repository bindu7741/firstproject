package bindu;
import java.util.Scanner;

public class NumberOfWords {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		String s1=new String(sc.nextLine());
		//getCountOfWords(s1);
		System.out.println(getCountOfWords (s1));
		
	}
	public static int getCountOfWords(String s1)
	{
		int count=0;
		String split[]=s1.split(" ");
		for(int i=0;i<split.length;i++)
		{
			if(!split[i].isEmpty())
			{
				count++;
			}
		}
	
		return count;
	}

}

package bindu;

import java.util.Scanner;

public class circle {
	public static void main(String args[])
	{
		double r,PI=3.14,perimeter,area;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter r value:");
		r=sc.nextDouble();
		perimeter=getPerimeter(r,PI);
		System.out.println("perimeter:"+perimeter);
		area=getArea(r,PI);
		System.out.println("area:"+area);
	}

	public static double getPerimeter(double r, double PI) {
		double perimeter = 2*PI*r;
		return perimeter;

	}

	public static double getArea(double r, double PI) {
		double area =PI*r*r;
		return area;
	}
}
package bindu;

import java.util.Scanner;

public class UniqueElement {
	public static void main(String args[]) {
		int elements[] = { 4, 5, 6, 7, 2, 9, 6, 8 };
		GetNumbers(elements);

	}

	public static void GetNumbers(int[] elements) {
		for (int i = 0; i < elements.length; i++) {
			int count = 0;
			for (int j = 0; j < elements.length; j++) {
				if (elements[i] == elements[j])
					count++;
			}
			if (count == 1)
				System.out.println(elements[i]);
		}
	}

}

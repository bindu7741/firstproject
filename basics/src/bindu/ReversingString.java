package bindu;
import java.util.Scanner;

public class ReversingString {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		String str;
		str=sc.nextLine();
		System.out.println(getWordsReverse(str));
		
	}
	public static String getWordsReverse(String str)
	{
		String revstr="";
		String s2[]=str.split(" ");
		for(int j=0;j<s2.length;j++)
		{
			String word=s2[j];
		for(int i=word.length()-1;i>=0;i--)
		{
		 revstr+=word.charAt(i);	
			
		}
			revstr+=" ";
		}
		return revstr;
	}
}

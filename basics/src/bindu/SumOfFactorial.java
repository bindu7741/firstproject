package bindu;

import java.util.Scanner;

public class SumOfFactorial {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number : ");
		int num = sc.nextInt();
		//System.out.println(getSum(num));
		
		System.out.println(aliquot(num));
	}

	public static int getSum(int num) {
		int sum = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0) {
				sum = sum + i + (num / i);
			}
		}
		return sum;
	}
	
	public static int aliquot(int num){
		int sum = 1, i;
		for(i = 2; i * i < num; i++){
			if(num % i == 0)
				sum += i + (num / i);
			
		}
		
		if(i * i == num)
			sum += i;
		
		return sum;
	}
}

package bindu;

import java.util.Scanner;

public class ReverseNumber {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		System.out.println(num);
		System.out.println(GetNumber(num));
	}
	public static int GetNumber(int num)
	{
		int sum=0;
	while(num>0)
	{
		int rem=num%10;
		sum=sum*10+rem;
		num=num/10;
	}
	return sum;
	}
}
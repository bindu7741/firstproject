package bindu;
import java.util.Scanner;

public class Discount 
{
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the price:");
		float price=sc.nextFloat();
		float sellingprice=GetSellingPrice(price);
		//GetDiscount(price);
		System.out.println("Selling price after discount:"+sellingprice);
		
	}
	public static float GetDiscount(float price)
	{
		float discount=0.0f;
		if(price>=0&&price<=10000)
		{
			discount=(price*10)/100;
			
		}
		else if(price>=10000&&price<=20000)
		{
			 discount=(price*20)/100;
			
		}
		else
		{
			discount=(price*25)/100;
		}
		return discount;
	}
		public static float GetSellingPrice(float price)
		{
			float discount=GetDiscount(price);
			return price-discount;
		}
}
	
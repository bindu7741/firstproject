package bindu;
import java.util.Scanner;
public class SecondMaxDigit {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		System.out.println("second max is:"+GetMax(num));
		
	}
	public static int GetMax(int num)
	{
		int max=0,digit=0,second_max=0;
		for(;num>0;num/=10)
		{
			digit=num%10;
			if(digit>max)
			{
				second_max=max;
				max=digit;
			}
			else if(digit<max&&digit>second_max)
				second_max=digit;
		}
			return second_max;
			}
	}



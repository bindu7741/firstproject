package bindu;
	public class OddPalindromes01 {
	    public static void main(String[] args) {
	        int num1 = 1500;
	        int num2 = 2000;
	        System.out.println(generateOddPalindromes(num1, num2));
	    }

	    public static String generateOddPalindromes(int start, int limit) {
	        //ADD YOUR CODE HERE
		String result="";
		if(start<=0||limit<=0)
		return "-1";
		else if(start>=limit)
		return "-2";
		else
	{
		for(int s=start;s<=limit;s++)
	{
		if(isPalindrome(s)&&isAllDigitsOdd(s))
		result=result+s+",";
		}
		if(result.isEmpty())
		return "-3";
		
	    }
		return result.substring(0,result.length()-1);
	}

	    public static boolean isPalindrome(int num) {
	        //ADD YOUR CODE HERE
		int reversenum=reverse(num);
		if(reversenum==num)
		
		return true;
		
		return false;
		}
		
		
		
	    	

	    public static int reverse(int num) {
	        //ADD YOUR CODE HERE
		int rev=0;
		while(num>0)
		{
		int rem=num%10;
		rev=rev*10+rem;
		num=num/10;
		
	   	 }
		return rev;
	}

	    public static boolean isAllDigitsOdd(int num) {
	        //ADD YOUR CODE HERE
		while(num>0)
	{
		int digit=num%10;
		if(digit%2==0&&digit!=0)
		return false;
		num/=10;
	}
		return true;
	    }
	}






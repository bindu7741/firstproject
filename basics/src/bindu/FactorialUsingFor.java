package bindu;
import java.util.Scanner;

public class FactorialUsingFor {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		System.out.println("Factorial of a number:"+GetFactorial(num));
		
		
	}
	public static int GetFactorial(int num)
	{
		int fact=1;
		for(int i=num;i>=1;i--)
		{
			 fact*=i;
		}
			
		return fact;	
			
	}
	

}

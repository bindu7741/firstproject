package bindu;
import java.util.Scanner;
public class Discount1 {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		float price=sc.nextFloat();
		GetDiscount(price);
		System.out.println(GetDiscount(price));
		
	}
	public static float GetDiscount(float price)
	{
		
		float discount=0.0f;
	
		if(price>=0&&price<=10000)
		{
			 discount=(price*10)/100;
			
		}
		else if(price>=10000&&price<=20000)
		{
			  discount=(price*20)/100;
			
		}
		else
		{
			 discount=(price*25)/100;
		}
		return price-discount;
	}
}

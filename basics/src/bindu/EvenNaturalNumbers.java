package bindu;
import java.util.Scanner;

public class EvenNaturalNumbers {
	public static void main(String args[])
	{
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter a number:");
	int num=sc.nextInt();
	System.out.println(SumOfEvenNaturalNumbers(num));
	
}
	public static int SumOfEvenNaturalNumbers(int num)
	{
		int sum=0,i=1;
		while(i<=num)
		{
			if(i%2==0)
			sum+=i;
			//System.out.println(sum);
			i++;
		}
			return sum;
			
		
	}
}

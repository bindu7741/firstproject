package bindu;

import java.util.Scanner;

public class Ispalindrome {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		// getNextPalindromeNumber(number);
		System.out.println(getNextPalindromeNumber(number + 1));
	}

	public static int getNextPalindromeNumber(int number) {
		while (!isPalindrome(number)) {
			number++;
		}
		return number;
	}

	public static boolean isPalindrome(int number) {
		return (number == (reverse(number)));

	}

	public static int reverse(int number) {
		int rev = 0, digit = 0;
		while (number > 0) {
			digit = number % 10;
			rev = rev * 10 + digit;
			number = number / 10;
		}
		return rev;

	}
}

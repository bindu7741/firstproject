package week5;
import java.util.Scanner;

public class SearchElement {
	
	public static void main(String args[])
	{
			System.out.println("Element is found at index:"+getSearchArrayElement());
	}
		
	public static int getSearchArrayElement()
		{
		
			Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size=sc.nextInt();
		int array[]=new int[size];
		
		System.out.println("Enter elements:");
			for(int i=0;i<size;i++)
		array[i]=sc.nextInt();
			
		System.out.println("enter the search element:");
		int search_element=sc.nextInt();
		
		for(int j=0;j<array.length;j++)
			{
				if(array[j]==search_element)
				{
					return j;
				}
			}
			return -1;
		}
}
	

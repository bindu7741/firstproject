package week6;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		String[] nd = scanner.nextLine().split(" ");
		scanner.next();
		int n = Integer.parseInt(nd[0]);

		int d = Integer.parseInt(nd[1]);

		int[] a = new int[n];

		String[] aItems = scanner.nextLine().split(" ");
		scanner.next();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 0; i < n; i++) {
			int aItem = Integer.parseInt(aItems[i]);
			a[i] = aItem;
		}
		int temp = a[0];
		for (int j = 0; j < d; j++) {
			for (int k = 0; k <= d; k++) {
				a[k] = a[k + 1];
				a[n-1] = temp;
				System.out.println(a[k]);
			}
		}

		scanner.close();
	}
}

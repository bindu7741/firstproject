package week3;

import java.util.Scanner;

public class BooleanCount {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		boolean x = sc.nextBoolean();
		boolean y = sc.nextBoolean();
		boolean z = sc.nextBoolean();
		// getTrueOrFalse(x,y,z);
		System.out.println(getTrueOrFalse(x, y, z));
	}

	public static boolean getTrueOrFalse(boolean x, boolean y, boolean z) {
		
		if(x && y && z)
			return false;
		else if (x && y || y && z || x && z)
			return true;
		else
		return false;
	}
	
	public static boolean countBoolean(boolean x, boolean y, boolean z){
	
		int count = 0;
		
		if(x)
			count++;
		if(y)
			count++;
		if(z)
			count++;
	
		return count == 2;
	
	}

}



package com.ex;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext ctx=getServletContext();
		String batch=ctx.getInitParameter("BatchName");
		String trainer=ctx.getInitParameter("Trainer");
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		out.print("Batch Name:" +batch +"<br> Trainer Name:" + trainer);
		ServletConfig cfg=getServletConfig();
		String id=cfg.getInitParameter("StudentId");
		String sname=cfg.getInitParameter("SName");
		out.print(id+ " " +sname);
		
	}

	

}

package com.ex;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletA extends HttpServlet {
	private static final long serialVersionUID = 1L;

    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String button=request.getParameter("bt");
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		if(button.equals("Go"))
		{
			String name=request.getParameter("t1");
			String branch=request.getParameter("t2");
			out.print("Welcome"+name);
			out.print("<a href='sb?v1="+name+"&v2="+branch+"'>click here</a>");
		}

}
}
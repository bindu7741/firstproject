import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

public class ClassDAO {
	public static Connection getConnection() throws SQLException, ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");	
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcex","root","root");
		return con; 
	}
	public static int insertStudent(Student st) throws SQLException, ClassNotFoundException
	{
	Connection c=getConnection();
	PreparedStatement ps= (PreparedStatement) c.prepareStatement("insert into student values(?,?,?)");
	ps.setInt(1, st.getId());
	ps.setString(2, st.getName());
	ps.setInt(3, st.getMarks());
	return ps.executeUpdate();
	
}
	public static int updateStudent(String s,int i) throws ClassNotFoundException, SQLException 
	{
		Connection c=getConnection();
		Statement st1=c.createStatement();
				return st1.executeUpdate("update student set name='"+s+"' where id="+i);
			}
	public static int deleteStudent(String s,int i) throws ClassNotFoundException, SQLException
	{
		Connection c=getConnection();
		Statement st2=c.createStatement();
		return st2.executeUpdate("delete from student where name='"+s+"' and id="+i );
		
	}
		
	}


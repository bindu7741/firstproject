import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class preparedset {
public static void main(String args[])throws ClassNotFoundException, SQLException
	
	{
		Class.forName("com.mysql.jdbc.Driver");	
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/day2","root","root");
		PreparedStatement pst=con.prepareStatement("insert into empl values(?,?,?,?)");
		pst.setInt(1, 101);
		pst.setString(2, "bindu");
		pst.setInt(3,30000);
		pst.setString(4, "CSE");
		pst.executeUpdate();
		pst.setInt(1, 102);
		pst.setString(2, "sindu");
		pst.setInt(3,30000);
		pst.setString(4, "ECE");
		pst.executeUpdate();
		pst.close();
		con.close();

}
}

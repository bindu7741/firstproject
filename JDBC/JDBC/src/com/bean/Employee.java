package com.bean;

public class Employee {
	private int eno;
	private String ename;
	private int esal;
	private String job;
	private int dno;
	private Department Dept;
	public Employee()
	{
		
	}
	public Employee(int eno, String ename, int esal, String job, int dno, Department dept) {
		super();
		this.eno = eno;
		this.ename = ename;
		this.esal = esal;
		this.job = job;
		this.dno = dno;
		Dept = dept;
	}
	public int getEno() {
		return eno;
	}
	public void setEno(int eno) {
		this.eno = eno;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public int getEsal() {
		return esal;
	}
	public void setEsal(int esal) {
		this.esal = esal;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public int getDno() {
		return dno;
	}
	public void setDno(int dno) {
		this.dno = dno;
	}
	public Department getDept() {
		return Dept;
	}
	public void setDept(Department dept) {
		Dept = dept;
	}
}
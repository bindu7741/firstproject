package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bean.Department;
import com.bean.Employee;

public class DepartmentDao {
	public static String getDepartmentName(int dno) throws ClassNotFoundException, SQLException
	{

		Class.forName("com.mysql.jdbc.Driver");	
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/day2","root","root");
		 PreparedStatement ps=con.prepareStatement("select dname from dept where dno=?"); 
		 ps.setInt(1,dno);
		 ResultSet s1=ps.executeQuery();
		 if(s1.next())
		 {
			 return s1.getString(1);
		 }
		 return null;
		 
	}
	public static List<Employee>getAllEmployeeCompleteDetails() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/day2", "root", "root");
		PreparedStatement ps = con.prepareStatement("select emp.*,dname from emp join dept on emp.dno=dept.dno");
		ResultSet rs = ps.executeQuery();
		Employee emp = null;
		Department d = null;
		List<Employee> empList = new ArrayList<>();
		while (rs.next()) {
		emp = new Employee();
		int id = rs.getInt(1);
		emp.setEno(id);
		emp.setEname(rs.getString(2));
		emp.setEsal(rs.getInt(3));
		emp.setJob(rs.getString(4));
		d = new Department();
		d.setDno(rs.getInt(5));
		d.setDname(rs.getString(6));
		emp.setDept(d);
		empList.add(emp);

	}
	
	return empList;
}
		public static String getEmployeeName(String ename) throws ClassNotFoundException, SQLException
		{

			Class.forName("com.mysql.jdbc.Driver");	
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/day2","root","root");
			 PreparedStatement ps=con.prepareStatement("select dname from dept join emp on emp.dno=dept.dno where ename=?"); 
			 ps.setString(1,ename);
			 ResultSet s1=ps.executeQuery();
			 if(s1.next())
			 {
				 return s1.getString(1);
			 }
			 return null;
			 
		}
		public static List<Employee> display() throws ClassNotFoundException, SQLException{
	      Class.forName("com.mysql.jdbc.Driver");
	      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/day2", "root", "root");
	      PreparedStatement ps = con.prepareStatement("select dept.dno,dname,ename,esal,job from dept join emp on dept.dno = emp.dno order by dno desc");
	      
	      ResultSet rs = ps.executeQuery();
	      Employee emp = null;
	      Department d = null;
	      List<Employee> empList = new ArrayList<>();
	     
	      while (rs.next()) {
	      
	      emp = new Employee();
	      
	      int id = rs.getInt(1);
	      emp.setEno(id);
	      emp.setEname(rs.getString(3));
	      emp.setEsal(rs.getInt(4));
	      emp.setJob(rs.getString(5));
	      
	      d = new Department();
	      d.setDno(rs.getInt(1));
	      d.setDname(rs.getString(2));
	      
	      emp.setDept(d);
	      empList.add(emp);

	      }
	      return empList;
	         
	          }
	
}


